-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-06-2024 a las 08:30:32
-- Versión del servidor: 10.5.25-MariaDB
-- Versión de PHP: 8.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `concejos_digesto`
--

DELIMITER $$
--
-- Procedimientos
--
$$

$$

$$

$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0001c6e06bbfc5ebcd69a4312269b8788e783ae7', '190.114.97.10', 1623679023, 0x5f5f63695f6c6173745f726567656e65726174657c693a313632333637393032333b),
('0006ba99b35f81de24af628ce3ecb2870bd48968', '200.123.45.113', 1711376671, 0x5f5f63695f6c6173745f726567656e65726174657c693a313731313337363637313b);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Descriptores`
--

CREATE TABLE `Descriptores` (
  `codigo` int(11) DEFAULT NULL,
  `descriptor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Descriptores`
--

INSERT INTO `Descriptores` (`codigo`, `descriptor`) VALUES
(1, 'Remises'),
(2, 'Vehiculos'),
(3, 'Transporte'),
(71, 'Fondos'),
(5, 'Tributos'),
(72, 'Ferrocarril'),
(7, 'Servicios Funebres'),
(8, 'Agua Corriente'),
(9, 'Cloacas'),
(10, 'Registro e Inspeccion'),
(11, 'Ordenamiento Ambiental'),
(12, 'Accion Social'),
(13, 'Servicios Sociales'),
(14, 'Arbol'),
(15, 'Policia Municipal'),
(460, 'Campana'),
(17, 'Cementerio'),
(18, 'Carnet de Conducir'),
(461, 'Relleno Sanitario'),
(73, 'Festival'),
(459, 'Exencion'),
(21, 'Alumbrado Publico'),
(462, 'Sepulcro'),
(23, 'Arte'),
(24, 'Artesano'),
(25, 'Becas'),
(26, 'Boleto'),
(27, 'Baldios'),
(28, 'Biblioteca'),
(29, 'Baile'),
(30, 'Convenios'),
(31, 'Combis'),
(32, 'Calle'),
(33, 'Centro Asistencial'),
(34, 'Creditos'),
(35, 'Clubes'),
(74, 'Gas Natural'),
(36, 'Canon'),
(37, 'Compras'),
(38, 'Carnaval'),
(39, 'Catastro'),
(40, 'Comision'),
(41, 'Cultura'),
(42, 'Concejo'),
(43, 'Cavas'),
(44, 'Contrato'),
(45, 'Codigo'),
(46, 'Concurso'),
(463, 'Suelo'),
(48, 'Contabilidad'),
(49, 'Clausura'),
(50, 'Contenedor'),
(51, 'Comercio'),
(52, 'Cyber'),
(53, 'Desagees'),
(54, 'Donacion'),
(55, 'Discapacidad'),
(56, 'Defensa Civil'),
(464, 'Plan Director'),
(58, 'Edificacion'),
(59, 'EICO'),
(60, 'Estacionamiento'),
(61, 'Escuela'),
(62, 'Eximicion'),
(63, 'Expropiacion'),
(64, 'Espectaculos'),
(65, 'Estatuto'),
(465, 'Juzgado'),
(67, 'Empleo'),
(466, 'Bolsas'),
(69, 'Ferias'),
(467, 'Peaje'),
(75, 'Gimnasio'),
(468, 'Usuarios'),
(469, 'Consumidores'),
(470, 'Jurisdiccion'),
(471, 'Promocion'),
(80, 'Hotel'),
(81, 'Hospital'),
(82, 'Intereses'),
(83, 'Iluminacion'),
(84, 'Institucion'),
(85, 'Impuesto'),
(86, 'Inmueble'),
(472, 'Nomenclatura'),
(88, 'Inundados'),
(89, 'Infraccion'),
(473, 'Empresas'),
(91, 'Inspeccion'),
(92, 'Iglesia'),
(93, 'Jubilados'),
(94, 'Jardin'),
(95, 'Juegos'),
(96, 'Junta'),
(97, 'Limpieza'),
(98, 'Licitacion'),
(99, 'Licencias'),
(100, 'Libre Deuda'),
(101, 'Liceo'),
(102, 'Loteos'),
(103, 'Locales'),
(104, 'Legales'),
(105, 'Municipal'),
(106, 'Multas'),
(107, 'Mejorado'),
(108, 'Mercado'),
(109, 'Matadero'),
(110, 'Moratoria'),
(111, 'Mercaderia'),
(112, 'Maquinaria'),
(113, 'Museo'),
(114, 'Mejoras'),
(115, 'Medicina'),
(116, 'Nombre'),
(117, 'Nichos'),
(118, 'Omnibus'),
(119, 'Obras'),
(120, 'Organigrama'),
(121, 'Oficina'),
(122, 'Personal'),
(123, 'Plaza'),
(124, 'Plazoleta'),
(125, 'Pavimentacion'),
(126, 'Presupuesto'),
(127, 'Plan'),
(128, 'Patente'),
(129, 'Parque'),
(130, 'Perros'),
(131, 'Plano'),
(144, 'Salud'),
(145, 'Sociedad'),
(132, 'Playa'),
(133, 'Pasajes'),
(134, 'Prensa'),
(135, 'Paso a Nivel'),
(136, 'Parroquia'),
(137, 'Policia'),
(138, 'Quiniela'),
(146, 'Servicios'),
(139, 'Reglamento'),
(140, 'Recursos'),
(141, 'Riego'),
(142, 'Ruidos Molestos'),
(143, 'Ropa de Trabajo'),
(147, 'Subsidio'),
(148, 'Semaforos'),
(149, 'Sueldos'),
(150, 'Secretarias'),
(151, 'Salarios'),
(152, 'Transporte Escolar'),
(153, 'Taxiflete'),
(154, 'Terreno'),
(155, 'Taxis'),
(156, 'Tasa'),
(218, 'Tarifa'),
(157, 'Tributaria'),
(158, 'Tapial'),
(159, 'Transito'),
(160, 'Turismo'),
(161, 'Urbanismo'),
(162, 'Vivienda'),
(163, 'Vereda'),
(164, 'Velatorio'),
(165, 'Vecinal'),
(166, 'Vecinos'),
(167, 'Animales'),
(168, 'Antena'),
(169, 'Barrio Cerrado'),
(170, 'Bancos'),
(171, 'Basura'),
(172, 'Centro Cultural'),
(173, 'Contratacion'),
(174, 'Celular'),
(175, 'Camping'),
(176, 'Combustible'),
(177, 'Chatarra'),
(178, 'Circulacion'),
(179, 'Cesion'),
(180, 'Condonacion'),
(181, 'Circo'),
(182, 'Comodato'),
(183, 'Confiteria Bailable'),
(184, 'Censo'),
(185, 'Deposito'),
(186, 'Deudas'),
(187, 'Desempleados'),
(188, 'Defensas'),
(189, 'Distincion'),
(190, 'Dieta'),
(191, 'Directorio'),
(192, 'Ex-Combatientes'),
(193, 'Emergencia'),
(194, 'Entretenimiento'),
(195, 'Faltas'),
(196, 'Financieras'),
(197, 'Gas-Oil'),
(198, 'H.C.M'),
(199, 'Industria'),
(474, 'Gas'),
(201, 'Kiosco'),
(202, 'Lactancia'),
(203, 'Matricula'),
(204, 'Mosquito'),
(205, 'Medio Ambiente'),
(206, 'Neumaticos'),
(207, 'Pavimento'),
(208, 'Pago'),
(209, 'Plusvalia'),
(210, 'Renegociacion'),
(211, 'Recorrido'),
(212, 'Regional'),
(213, 'Reductor'),
(214, 'Radicacion'),
(215, 'Seguridad'),
(216, 'Software'),
(217, 'Senales'),
(219, 'Telefono'),
(220, 'Carribares'),
(221, 'Espacio Verde'),
(222, 'Agente'),
(223, 'Boxeo'),
(490, 'Gastos'),
(225, 'Laboral'),
(226, 'Quincho'),
(227, 'Balneario'),
(228, 'Diploma'),
(229, 'Premio'),
(230, 'Bicicletas'),
(231, 'Obrador'),
(232, 'Comite'),
(475, 'Garrafa'),
(234, 'Permuta'),
(235, 'Lotes'),
(236, 'Asteom'),
(237, 'Energia'),
(238, 'Consorcio'),
(239, 'Caminos'),
(240, 'Autopista'),
(241, 'Regularizacion'),
(242, 'Construccion'),
(287, 'Registro'),
(243, 'Cordon'),
(244, 'Cuneta'),
(245, 'Propiedad'),
(246, 'Propiedad Horizontal'),
(247, 'Instalaciones'),
(248, 'VIALIDAD'),
(249, 'ORGANOS'),
(288, 'Bienes'),
(289, 'Responsabilidad'),
(250, 'Proveedores'),
(251, 'Residuos Peligrosos'),
(252, 'Ninos/as'),
(253, 'Derechos'),
(254, 'Paso del salado'),
(255, 'Telecomucicaciones'),
(256, 'Deporte'),
(257, 'Apoderado'),
(476, 'Difusion'),
(259, 'Alcohol'),
(260, 'Codigo'),
(261, 'Lecop'),
(262, 'Fonavi'),
(263, 'Compensacion'),
(264, 'Venta'),
(265, 'Rural'),
(266, 'Tierra'),
(290, 'Pozo ciego'),
(267, 'Trabajo'),
(268, 'Ambulante'),
(269, 'Local'),
(270, 'L Bis'),
(271, 'Agua'),
(272, 'Refugio'),
(273, 'Garita'),
(274, 'Boletin'),
(275, 'Prohibicion'),
(276, 'Logo'),
(277, 'Centro comunitario'),
(278, 'Anticipo'),
(279, 'Rampas'),
(280, 'Locacion'),
(281, 'Ancho'),
(282, 'Ceride'),
(283, 'Menor'),
(284, 'Inversion'),
(285, 'Cuenta corriente'),
(286, 'Cementerio privado'),
(291, 'Desafectacion'),
(477, 'Puente'),
(293, 'Escrituracion'),
(294, 'Delegacion'),
(295, 'Vivero'),
(296, 'Unidad'),
(297, 'Intendencia'),
(298, 'Tesorero'),
(299, 'Concejales'),
(300, 'Geriatrico'),
(301, 'Prestamo'),
(302, 'Teatro'),
(478, 'Direccion Nacional Vialidad (D.N.V.)'),
(304, 'Habilitacion'),
(305, 'Guardavidas'),
(306, 'Director'),
(307, 'Iniciativa privada'),
(308, 'Arbitros'),
(309, 'Seguro'),
(310, 'Alquiler'),
(311, 'Senalizacion'),
(312, 'Impacto ambiental'),
(313, 'Alta tension'),
(314, 'Conducir'),
(315, 'Mujer'),
(316, 'Paradas'),
(317, 'Barrio'),
(318, 'Via publica'),
(319, 'Banos'),
(320, 'Residuos patologicos'),
(321, 'Folklore'),
(322, 'Hipermercado'),
(323, 'Ramas'),
(324, 'Publicidad'),
(491, 'codigo'),
(326, 'Umirsu'),
(327, 'Residuos'),
(328, 'Educacion'),
(329, 'Carteleria'),
(330, 'Discriminacion'),
(331, 'Zona'),
(332, 'Porquerizas'),
(333, 'Pirotecnia'),
(334, 'Peatonal'),
(359, 'Ensanche'),
(336, 'Cloacales'),
(337, 'Busto'),
(487, 'Audiencia Publica'),
(339, 'Concesion'),
(360, 'Casilla'),
(361, 'Veterano'),
(340, 'Fondo'),
(341, 'Pasaje'),
(362, 'Ciclista'),
(342, 'Centro comercial'),
(488, 'Coro'),
(344, 'Ordenanza'),
(345, 'Humo'),
(346, 'Topadora'),
(347, 'Cigarrillo'),
(348, 'Revocacion'),
(349, 'Loyola'),
(350, 'Facultad'),
(351, 'Asesoramiento'),
(363, 'Representacion'),
(352, 'Incapacidad'),
(353, 'Dominio'),
(354, 'Publico'),
(355, 'Enajenacion'),
(356, 'Informe'),
(357, 'Escalafon'),
(358, 'Escolar'),
(364, 'Sellado'),
(489, 'Historia'),
(366, 'Cadeteria'),
(367, 'Carnet'),
(368, 'Alicuota'),
(369, 'Planta'),
(370, 'Liquidos'),
(371, 'T.G.I'),
(372, 'C.I.P.D'),
(373, 'Patrimonio'),
(374, 'Arqueologia'),
(375, 'Violencia'),
(435, 'Acueducto'),
(492, 'Monumento'),
(377, 'Compraventa'),
(378, 'Intendente'),
(379, 'Relevamiento'),
(380, 'Sanitario'),
(381, 'Horizontal'),
(382, 'Electrica'),
(383, 'Denuncia'),
(384, 'Funcionarios'),
(385, 'Urbano'),
(386, 'Escritura'),
(387, 'Cine'),
(388, 'Brigada Femenina'),
(389, 'Primeros auxilios'),
(390, 'Soldados'),
(391, 'Socorrismo'),
(392, 'Colinia de Vacaciones'),
(479, 'Productos Fitosanitarios'),
(480, 'Subasta'),
(395, 'Laboratorios'),
(396, 'Bonificacion'),
(397, 'Direccion Provincial Vialidad'),
(481, 'Procedimiento'),
(399, 'Servicio Contenedores'),
(400, 'Prorroga'),
(401, 'Club Ciclista'),
(402, 'Institutos'),
(403, 'Refinanciacion Deuda'),
(404, 'Incorporacion'),
(405, 'Telefonia'),
(406, 'Calzadas'),
(407, 'Cuenca Arroyo Malaquias-Los Troncos'),
(408, 'Asfalto'),
(409, 'Asociacion Cooperadora'),
(410, 'Canal Roverano'),
(411, 'Direccion Pcial. Vivienda y Urbanismo'),
(412, 'Urbanizacion'),
(413, 'Ampliacion'),
(414, 'Red Cloacal'),
(415, 'SUM'),
(416, 'Transferencia'),
(417, 'FIRMA DIGITAL'),
(418, 'Pegamentos'),
(419, 'Presupuesto Participativo'),
(420, 'Informacion Publica'),
(421, 'Pileta Decantadora'),
(422, 'Violencia Laboral'),
(423, 'Adhesion'),
(424, 'Elementos Opticos'),
(482, 'Codigo de Edificacion'),
(426, 'Servicios Sanitarios'),
(427, 'Identificacion'),
(428, 'Protocolo'),
(429, 'Regiones'),
(493, 'Comision Codigo Edificacion (C.C.E.)'),
(431, 'Fondos no reintegrables'),
(432, 'Equipamiento'),
(433, 'Emprendimientos Locales'),
(434, 'Alimentos'),
(436, 'Preventiva'),
(438, 'Codigo Municipal de Faltas'),
(439, 'Movilidad'),
(440, 'Haberes previsionales'),
(441, 'Programa'),
(442, 'Subdivision'),
(443, 'Traslado'),
(444, 'Taller'),
(445, 'Agua potable'),
(446, 'Ruta'),
(447, 'Autovia'),
(448, 'Desmalezamiento'),
(449, 'IAPOS'),
(450, 'E.P.E.'),
(451, 'SAMCo'),
(452, 'Conmemoracion'),
(453, 'Centro de Salud'),
(454, 'Lavadero'),
(455, 'Criadero'),
(456, 'Centro Civico'),
(457, 'SUPERFICIE'),
(458, 'Espacio Aereo'),
(483, 'Armas'),
(484, 'Institucion de dia'),
(485, 'Cinerario'),
(486, 'Internet'),
(494, 'Aeropuerto'),
(495, 'Area Metropolitana'),
(496, 'Medicamentos'),
(497, 'Asueto/Dia no Laborable'),
(498, 'Indemnizacion'),
(499, 'Artistas'),
(500, 'Paridad'),
(501, 'Genero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Estructuratematicanivel1`
--

CREATE TABLE `Estructuratematicanivel1` (
  `indice1` int(11) NOT NULL,
  `descripcion1` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Estructuratematicanivel1`
--

INSERT INTO `Estructuratematicanivel1` (`indice1`, `descripcion1`) VALUES
(1, 'Introducción'),
(2, 'Institucional'),
(3, 'Legislación General'),
(4, 'Economía y Hacienda'),
(5, 'Obras Públicas y Urbanismo'),
(6, 'Tránsito y Transporte'),
(7, 'Medio Ambiente e Higiene');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Estructuratematicanivel2`
--

CREATE TABLE `Estructuratematicanivel2` (
  `indice1` int(11) NOT NULL,
  `indice2` int(11) NOT NULL COMMENT '	',
  `descripcion2` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Estructuratematicanivel2`
--

INSERT INTO `Estructuratematicanivel2` (`indice1`, `indice2`, `descripcion2`) VALUES
(1, 1, 'Introducción'),
(2, 1, 'Constitución Nacional'),
(2, 2, 'Constitución Provincial'),
(2, 3, 'Ley Orgánica de Municipalidades'),
(2, 4, 'Estatuto y Escalafón del Personal Municipal'),
(2, 5, 'Régimen de Licencias y Franquicias'),
(2, 6, 'Reglamento Interno del H.C.M.'),
(2, 7, 'Ley 6767'),
(2, 8, 'Ley 11257'),
(2, 9, 'Varios'),
(3, 1, 'Código Municipal de Faltas'),
(3, 2, 'Declaración patrimonial de bienes'),
(3, 3, 'Iniciativa Popular'),
(3, 4, 'Consejo Municipal de Seguridad'),
(3, 5, 'Reglamentación de actividades'),
(3, 6, 'Plan de Empleo Local'),
(3, 7, 'Programas'),
(3, 8, 'Varios'),
(4, 2, 'Ordenanza Tributaria'),
(4, 3, 'Régimen de Compras'),
(4, 4, 'Eximición de Tasas Municipales'),
(4, 5, 'Contribución por Mejoras'),
(4, 6, 'Alícuota por Plusvalía'),
(4, 7, 'Contrataciones'),
(4, 8, 'Regularización de Deudas'),
(4, 9, 'Varios'),
(5, 1, 'Plan Director'),
(5, 2, 'Código de Edificación'),
(5, 3, 'Alumbrado Publico'),
(5, 4, 'Servicios Públicos'),
(5, 5, 'Obras Publicas'),
(5, 6, 'Barrios Cerrados'),
(5, 7, 'Varios'),
(6, 1, 'Reglamentación del transito'),
(6, 2, 'Servicio de transporte urbano de pasajeros'),
(6, 3, 'Remis'),
(6, 4, 'Taxis'),
(6, 5, 'Transportes especiales'),
(6, 6, 'Taxi-flete'),
(6, 7, 'Sentido circulación de calles'),
(6, 8, 'Varios'),
(7, 1, 'Higiene Publica'),
(7, 2, 'Régimen de Cementerios'),
(7, 3, 'Espectáculos Públicos'),
(7, 4, 'Parque Industrial'),
(7, 5, 'Animales'),
(7, 6, 'Emergencias y Catástrofes'),
(7, 7, 'Reglamentación de cartelería'),
(7, 8, 'Pirotecnia'),
(7, 9, 'Código de Preservación forestal'),
(7, 10, 'Residuos Peligrosos'),
(7, 11, 'Espacios Verdes'),
(7, 12, 'Vendedores Ambulantes'),
(7, 13, 'Ferias'),
(7, 14, 'Bromatología'),
(7, 15, 'Prohibiciones - Restricciones'),
(7, 16, 'Impacto Ambiental'),
(7, 17, 'Carribares'),
(7, 18, 'Varios'),
(8, 1, 'Vecinales'),
(8, 2, 'Fomento Educativo'),
(8, 3, 'Actividades Culturales'),
(8, 4, 'Discapacidades'),
(8, 5, 'Actividad deportiva'),
(8, 6, 'Turismo'),
(8, 7, 'Varios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Normas`
--

CREATE TABLE `Normas` (
  `tipo` int(11) NOT NULL,
  `numero` int(11) NOT NULL COMMENT '			',
  `expedientechm` varchar(250) DEFAULT NULL,
  `fechasancion` date DEFAULT '0000-00-00',
  `expedientedem` varchar(250) DEFAULT NULL,
  `fechapromulgacion` date DEFAULT '0000-00-00',
  `origen` varchar(100) DEFAULT NULL,
  `autor` varchar(250) DEFAULT NULL,
  `contenido` longtext DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `caracter` varchar(255) DEFAULT NULL,
  `alcance` varchar(255) DEFAULT NULL,
  `archivo` varchar(250) DEFAULT NULL,
  `archivoord` varchar(250) DEFAULT NULL,
  `nrocaja` int(11) DEFAULT 0,
  `nroorden` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Normas`
--

INSERT INTO `Normas` (`tipo`, `numero`, `expedientechm`, `fechasancion`, `expedientedem`, `fechapromulgacion`, `origen`, `autor`, `contenido`, `observaciones`, `caracter`, `alcance`, `archivo`, `archivoord`, `nrocaja`, `nroorden`) VALUES
(4, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'CONSTITUCION DE LA PROVINCIA DE SANTA FE. ', NULL, NULL, 'Permanente', 'I0000001.pdf', NULL, 0, NULL),
(4, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'CONSTITUCION NACIONAL.', NULL, NULL, 'Permanente', 'I0000002.pdf', NULL, 0, NULL),
(4, 3, NULL, '1963-10-16', NULL, '1963-10-16', 'DECRETO H.C.M. N 01/63.', 'H.C.M.', 'REGLAMENTO INTERNO DEL HONORABLE CONCEJO MUNICIPAL. ', 'ANEXO I-ESTRUCTURA ORGANICA (DEC. N 21/12 Y 03/15).', 'GENERAL', 'Permanente', 'I0000003.pdf', NULL, 0, NULL),
(3, 106, '4143', '2006-03-28', NULL, NULL, 'H.C.M.', 'RIPOLL - ZUANET - RIPOLL - LUJAN (P.J.)', 'FACULTESE A LA PRESIDENCIA DEL H.C.M., A REALIZAR GESTIONES ANTE EL MINISTERIO DE SALUD DE LA PROVINCIA SOLICITANDO UNA AUDIENCIA CON LA SRA. MINISTRA CON EL OBJETO DE PLANTEARLE DIVERSOS TEMAS INHERENTES A LA SITUACION DEL SAMCO DE SANTO TOME Y CENTROS A', NULL, NULL, NULL, 'R0000106.pdf', NULL, 109, '4668'),
(3, 107, NULL, '2007-03-27', NULL, NULL, 'NOTA RECIBIDA N 2681 (06/11/06) Y N 2724 (26/02/07)', 'H.C.M.', 'RECHACENSE POR IMPROCEDENTES LOS RECURSOS DE RECONSIDERACION (Y/O REVOCATORIA) Y/O NULIDAD Y/O APELACION Y/U OTROS PLANTEADOS CONTRA LA ORDENANZA N 2571/06. ', NULL, NULL, NULL, 'R0000107.pdf', NULL, 0, NULL),
(3, 108, '4675', '2008-03-26', NULL, NULL, 'H.C.M.', 'LOSTUMBO - MARTINEZ (P.S.)', 'DISPONESE QUE EL D.E.M. EJECUTE UN PLAN DE TRABAJO PARA DEMARCAR, NUEVAMENTE, LAS SENDAS PEATONALES EN LAS AVENIDAS 7 DE MARZO Y NTRA. SRA. DE LUJAN. ', NULL, NULL, NULL, 'R0000108.pdf', NULL, 119, '4993');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Normasdescriptor`
--

CREATE TABLE `Normasdescriptor` (
  `codigo` int(11) NOT NULL,
  `norma` int(11) NOT NULL,
  `descriptor` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Normasdescriptor`
--

INSERT INTO `Normasdescriptor` (`codigo`, `norma`, `descriptor`) VALUES
(1, 1, '139'),
(1, 53, '59'),
(1, 430, '165');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Relacionentrenormas`
--

CREATE TABLE `Relacionentrenormas` (
  `idrelacion` int(11) NOT NULL,
  `tiponorma` int(11) NOT NULL,
  `nronorma` int(11) NOT NULL,
  `relacion` int(11) NOT NULL,
  `tiponormar` int(11) NOT NULL,
  `nronormar` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `observacion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Relacionentrenormas`
--

INSERT INTO `Relacionentrenormas` (`idrelacion`, `tiponorma`, `nronorma`, `relacion`, `tiponormar`, `nronormar`, `fecha`, `observacion`) VALUES
(1, 1, 2208, 5, 1, 1874, '2001-10-04', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Relacionesentrenormas`
--

CREATE TABLE `Relacionesentrenormas` (
  `codigo` int(11) NOT NULL,
  `relacion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Relacionesentrenormas`
--

INSERT INTO `Relacionesentrenormas` (`codigo`, `relacion`) VALUES
(1, 'Modifica a'),
(2, 'Deroga en forma parcial a'),
(3, 'Deroga en forma total a'),
(4, 'Reglamenta a'),
(5, 'Relacionada a'),
(51, 'Modificada por'),
(52, 'Derogada en forma parcial por'),
(53, 'Derogada en forma total por'),
(54, 'Reglamentada por'),
(55, 'Relacionada con');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tiposdenormas`
--

CREATE TABLE `Tiposdenormas` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `Tiposdenormas`
--

INSERT INTO `Tiposdenormas` (`codigo`, `nombre`) VALUES
(1, 'Ordenanza'),
(2, 'Decreto'),
(3, 'Resolucion'),
(4, 'Institucional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(40) NOT NULL,
  `last_login` datetime NOT NULL,
  `habilitado` int(11) NOT NULL DEFAULT 1 COMMENT 'El nivel 0 es usuario comun. nO tiene acceso a la configuraci',
  `nivel` int(11) DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `nombre`, `email`, `last_login`, `habilitado`, `nivel`) VALUES
(1, 'danilo', 'c8d547c9387ad3df71118c9c759e00d1', 'Danilo Leyendeker', 'dleyendeker@gmail.com', '2019-05-28 23:03:17', 1, 1),
(2, 'alta', '2cec7f6db1783025504e3696c0201306', 'Alta Digesto', 'altadogesto@concejosantotome.gob.ar', '2020-08-20 22:04:08', 1, 99);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `Estructuratematicanivel1`
--
ALTER TABLE `Estructuratematicanivel1`
  ADD PRIMARY KEY (`indice1`);

--
-- Indices de la tabla `Relacionentrenormas`
--
ALTER TABLE `Relacionentrenormas`
  ADD PRIMARY KEY (`idrelacion`);

--
-- Indices de la tabla `Relacionesentrenormas`
--
ALTER TABLE `Relacionesentrenormas`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `Tiposdenormas`
--
ALTER TABLE `Tiposdenormas`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Relacionentrenormas`
--
ALTER TABLE `Relacionentrenormas`
  MODIFY `idrelacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1380;

--
-- AUTO_INCREMENT de la tabla `Tiposdenormas`
--
ALTER TABLE `Tiposdenormas`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
