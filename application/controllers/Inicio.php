<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    
    public function loginabm()       
    {
		$datos['titulo']="ABM Digesto Santo Tome";
		$this->load->view('login/headerlogin',$datos);
		$this->load->view('login/login');
		this->load->view('login/footer');
    }
		
    /**
    * Logue al Usuario.
    * 
    * Llama a la funcion check_database 
    * 
    * @author Leyker
    * @return boolean true si es correcto
    */
    public function verifylogin()
    {
        if($this->check_database() == TRUE)
        {
          redirect('abmnorma', 'refresh');

        }else{
          $this->session->set_flashdata('error_msg', 'Error al loguearse. Verifique los datos.');
          redirect(base_url(), 'refresh');
        }
    }
        
    /**
    * Cierra la sesión abierta.
    * y te lleva al inicio
    */
    public function logout()
    {
        $this->check_log();
        $this->session->sess_destroy();
        redirect (base_url(),'refresh');
    }

    /**
    * Chequea en la BBDD.
    * 
    * Control los datos del usuario
		* 
		
    * @author Leyker
    * @param string $password La clave del usuario 
    * @return boolean true si es correcto
    */
    function check_database()
    {
        $this->load->model('userdb','',TRUE); 
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');
        $password=$this->input->post('password');
        //query the database
        $result = $this->userdb->login($username, $password);

        if($result)
        {  
            $sess_array = array();
            $this->load->model('userdb',true);
            foreach($result as $row)
            {
                $sess_array = array(
                'id' => $row->id,
                'nombrecompleto' => $row->nombre,
                'email' => $row->email,
                'username' => $row->username,
                'nivel'=> $row->nivel,
				'habilitado'=> $row->habilitado,
				'email'=> $row->email,
                'logueado'=>TRUE,
                );
                $this->session->set_userdata($sess_array);
            }
            return TRUE;
        }else{
            $this->session->unset_userdata('userdata');
            return FALSE;
        }
    }

    /**
    * Chequea que la sesión esté iniciada
    * Usarla antes de ejecutar un método de un controller
    * 
    * @author Leyker
    */
    private function check_log (){
        if($this->session->userdata('logueado') == FALSE){
            redirect(base_url(), 'refresh');    
        }
    }

    /*Controlle de Inicio.
    */
    public function busqueda()
	{
        $this->load->model('digestodb','',TRUE);
        // check if the trabajo exists before trying to edit it
        $this->load->library('form_validation');

        //$this->form_validation->set_rules('busqueda','Busqueda');
        //$this->form_validation->set_rules('busquedanorma','Busqueda');  
        $uno=$this->input->post('busqueda');
        $dos=$this->input->post('busquedanorma');
        $desde=$this->input->post('fechadesde');
        $hasta=$this->input->post('fechahasta');

        if(($uno<>null) or ($uno<>"") or ($dos<>null) or ($dos<>"")){
            if(!empty($_POST['tipo_norma'])){
                // Ciclo para mostrar las casillas checked checkbox.
                foreach($_POST['tipo_norma'] as $selected){
                    $tipo[]=$selected;
                }
            }else{
                $tipo=null;
            }
            $this->load->model('digestodb','',TRUE);
            if ($this->input->post('busquedanorma')<>""){
                $resnormas = $this->digestodb->busqueda_norma_numero($this->input->post('busquedanorma'),$tipo,$desde,$hasta);    
            }else{
                $resnormas = $this->digestodb->busqueda_norma($this->input->post('busqueda'),$tipo,$desde,$hasta);
            }
            $data['listaNormas'] = json_decode(json_encode($resnormas), True);
            $data['contador']=count($resnormas);
        }  

        $data['all_tipo'] = $this->digestodb->get_tipo();
        $data['titulo']="Digesto 0.1";
        $data['encabezado']="Digesto Municipal de Santo Tomé -  V. 0.1";
        $data['_view'] = $this->load->view('principal/busqueda', $data, true);
              
        $this->load->view('template/header',$data);
        $this->load->view('principal/main');
        $this->load->view('template/footer');
    }  

    public function detalles($norma,$tipo){
        $this->load->model('digestodb','',TRUE);
        $relaciones = $this->digestodb->busqueda_relaciones($norma,$tipo); 
        $relacionesinv = $this->digestodb->busqueda_relacionesinversas($norma,$tipo);     
        $norma_v = $this->digestodb->devuelve_norma($norma,$tipo);
        $estructuratematica=$this->digestodb->devuelve_estructuratematica($norma,$tipo);
        $data['relaciones'] = json_decode(json_encode($relaciones), True);
        $data['relacionesinversas'] = json_decode(json_encode($relacionesinv), True);
        $data['norma'] = json_decode(json_encode($norma_v), True);
        $data['estructuratematica'] = json_decode(json_encode($estructuratematica), True);
        $this->load->view('template/header',$data);
        $this->load->view('principal/detalles');
        //$this->load->view('template/footer');
    }
}
