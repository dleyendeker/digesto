# Digesto Legal 1.0 - Codeigniter


## ¿Qué es?
Es un desarrollo web para poder hacer publica la información legal de cualquier municipio
Al día de hoy podés (como usuario):
-Consultar Normas, reglamentaciones, etc, realizando búsquedas por texto, numero de norme, etc.
-Bajar PDF de las normas en cuestión.
Al día de hoy podés (como administrador):
-ABM de Normas


## Informacion técnica
Realizado en Codeigniter 3. Necesita conexión a Mysql o Maríadb. Versión de Php: 7.4


## Estructura de la aplicación.

* Carpeta "normas": Se subiran los docuemntos al momento de dar de alta una Normas

* Carpeta "assets": Posee los domcumentos css y js como asi tambien las imagenes.

* Carpeta abm-norma": Posee una extructura extra de codeigniter para el abm de normas. El ingreso al mismo se realiza desde digesto/abm-norma/index.php/index.php/ son usuario y contraseña. En un servidor local: http://localhost/digesto/abm-norma



## Instalación y puesta en marcha

* Clonar el repo: git clone https://gitlab.com/dleyendeker/digesto.git

* Necesario: servidor web apache. Php 7.4. Mysql o Maridb

* Correr el script concejos_digesto_BBDD.sql

* Para coenctar la bbdd, deberá modificar el archivo database.php que se encuentra en la carpeta application->config. En la linea 76 encontrará un array. Deberá modificar los campos 'hostname','usernam','password','database'.

* Por ultimo, modificar de ser necesario el archivo config.php de la carpeta application->config, en la linea 26. Allí encontrará el valor config['base_url'] que deberá copnfigurar con el valor de su servidor web.


## Aclaración:

Usuario administrador cagda: admin - admin
Link administracion: /abm-norma/


## Contacto y web de ejemplo disponible:

Autor: Danilo Leyendeker

Correo: dleyendeker@gmail.com

Digesto desarrollado para la Municipalidad de Santo Tomé, pcia. de Santa Fe, Argentina.

